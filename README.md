# Management of Teaching and Scientific Meetings

This project, developed using Spring, Thymeleaf, and PostgreSQL, serves as a comprehensive tool for managing teaching and scientific meetings at **Faculty of Computer Science and Engineering - Skopje**. It integrates with a common model to access faculty data efficiently.

## New Features

- **Current Title of Professor**: Save the current title of a professor as a string and display the position and professor's name, so that a professor with a new title can view their past title for a specific meeting.
- **Request for Scheduling**: A form is available for members to generate requests to add meeting items. Admins can view these requests and schedule them for an existing meeting.

- **AI-Supported Import (Partially Done)**: A prompt generates SQL to populate the database with meetings/items based on a Word document of a past meeting.

- **Meeting Leading**: The meeting leader (ML) has "read-only" access. Topics appear on the left side of the window, and by clicking a topic, the ML sees the topic details on the right side. MLs can add votes and discussion.

  - Meetings now include a boolean property `isVotable`. If not votable, the meeting status changes to "finished" when the finish button is clicked. Otherwise, it checks if votes are not empty, saves the votes, and finishes the meeting.

- **HTML Meeting Report**: Generate a meeting report for each meeting with emphasis on voting results and discussions that took place.

## Features

### CRUD Operations

- **Meetings**: Create, read, update, and delete meetings. Each meeting can have multiple topics discussed.
- **Topics**: Manage topics within meetings, allowing CRUD operations and detailed interaction features for professors.

### Professor Interaction

- **Comments and Files**: Professors can leave comments on topics, upload/download files, and share document links directly related to meeting discussions.
- **Permissions**: Role-based access control ensures that only meeting holders have full CRUD capabilities, maintaining data integrity and security.

### Tabular Display and Filtering

- **Meetings Tab**: View all past and future meetings in a paginated table format (5 meetings per page). Use Pageable to navigate through meetings and filter them by date range.
- **Professors Tab**: Browse through a paginated list of professors, with the ability to filter by their involvement in topics (e.g., mentions, commissions, mentoring). Detailed views provide insights into specific topics relevant to each professor.
- **Students Tab**: Similar to the Professors tab but focused on student involvement in topics, limited to mentions without commission or mentoring roles.

### Dockerized Deployment

- **Seamless Deployment**: Utilizes Docker and Docker Compose to eliminate environment inconsistencies. Follow the steps below to set up the application.

## Accessing the Application

### Prerequisites

- Ensure the COMMON MODEL application is running to set up the necessary database schemas.
- Docker

### Steps to Run

1. **Start COMMON MODEL Application**:

   - Navigate to the directory of your COMMON MODEL project.
   - Initialize the necessary services to create the database schema:
     ```bash
     cd path/to/common-model
     docker-compose up -d finki-db
     ```
     This will create the `finki_services_db` container and the `common-model_finki_services_net` network.

2. **Initialize Database for TSM Application**:

   - Navigate to the directory of your TSM (Teaching and Scientific Meetings) project.
   - Launch the PostgreSQL container for the faculty database using Docker Compose:
     ```bash
     cd path/to/tsm-app
     docker-compose up -d
     ```

3. **Run the TSM Application**:

   - Create a `.env` file in the TSM project root with the following configuration:
     ```bash
     DB_PASSWORD=commonModelPassword
     ```
   - Build and start the Docker containers for the TSM application:
     ```bash
     docker-compose up -d --build
     ```

4. **Access the TSM Application**:
   - Open your web browser and navigate to:
     ```bash
     http://localhost:8000
     ```

## Inserting Data into the `student` Table

### Access PostgreSQL Database

1. **Open your terminal or command prompt.**
2. **Connect to the PostgreSQL database using the `psql` command:**
   ```bash
   psql -U finki_admin -d finki-services-db
   ```
   Once connected to the database, you can insert data using the INSERT INTO SQL command.

**Example for inserting multiple rows into the student table:**

```bash
INSERT INTO student (student_index, email, last_name, name, parent_name, study_program_code)
VALUES
    ('213201', 'jane.panov@students.finki.ukim.mk', 'Panov', 'Jane', 'Vlatko', 'PIT23'),
    ('213016', 'hristijan.stefov@students.finki.ukim.mk', 'Stefov', 'Hristijan', 'Viktor', 'PIT23'),
    ('213079', 'panche.prendjov@students.finki.ukim.mk', 'Prendjov', 'Panche', 'Baze', 'PIT23'),
    ('213118', 'matej.todorovski@students.finki.ukim.mk', 'Todorovski', 'Matej', 'Test', 'PIT23');
```

3. **Verify Data Insertion**

- After executing the INSERT statement, you should see a confirmation message indicating the number of rows inserted (INSERT 0 4 for four rows in this example).

4. **Commit Changes**

- By default, PostgreSQL transactions are committed automatically after each SQL command that modifies data. If you need to manually commit changes (in case of explicit transactions), use the `COMMIT;` command.

5. **Disconnect from PostgreSQL**

To disconnect from the PostgreSQL database, type `\q` and press Enter.

## Notes

- Ensure the COMMON MODEL application is running before starting both the database container and the TSM application container to establish proper network connections.

## Contributors

- Hristijan Stefov
- Panche Prendjov
- Matej Todorovski
- Jane Panov