package mk.ukim.finki.wp.nns.config;

import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ThymeleafConfig {
    @Bean
    public Java8TimeDialect java8TimeDialect() {
        return new Java8TimeDialect();
    }
}