package mk.ukim.finki.wp.nns.model.enumerations;

public enum AppRole
{
    ADMIN, PROFESSOR, GUEST;

    public String roleName()
    {
        return "ROLE_" + this.name();
    }
}
