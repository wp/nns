package mk.ukim.finki.wp.nns.model.enumerations;


import lombok.Getter;

@Getter
public enum ProfessorTitle
{
    TUTOR(false), TEACHING_ASSISTANT(false),
    ASSISTANT_PROFESSOR(true), ASSOCIATE_PROFESSOR(true), PROFESSOR(true), RETIRED(true),
    ELECTED_ASSISTANT_PROFESSOR(true), ELECTED_ASSOCIATE_PROFESSOR(true), ELECTED_PROFESSOR(true),
    EXTERNAL_EXPERT(false);

    private boolean isProfessor;

    ProfessorTitle(boolean isProfessor)
    {
        this.isProfessor = isProfessor;
    }

    public String typeName()
    {
        switch(this.name())
        {
            case "TUTOR":
                return "Демонстратор";
            case "TEACHING_ASSISTANT":
                return "Асистент";
            case "ASSISTANT_PROFESSOR":
                return "Доцент";
            case "ASSOCIATE_PROFESSOR":
                return "Вонреден професор";
            case "PROFESSOR":
                return "Редовен професор";
            default:
                return "Професор";
        }
    }
}
