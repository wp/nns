package mk.ukim.finki.wp.nns.model.enumerations;

public enum TopicCategory
{
    STAFF, MASTER, PHD, TEACHING, FINANCE, OTHER
}
