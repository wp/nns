package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.TopicComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<TopicComment, Long>
{

}
