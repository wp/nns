package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.CommissionMember;
import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.enumerations.CommissionMemberType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CommissionMemberRepository extends JpaRepository<CommissionMember,String>
{
    Optional<CommissionMember> findCommissionMemberByProfessorAndType(Professor professor, CommissionMemberType type);

    List<CommissionMember> findAllByProfessor(Professor professor);
}
