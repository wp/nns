package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.TeachingAndScientificMeeting;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface MeetingRepository extends JpaRepository<TeachingAndScientificMeeting, String>
{
        List<TeachingAndScientificMeeting> findAllByOrderByDateDesc();

        Page<TeachingAndScientificMeeting> findAllByOrderByDateDesc(Pageable pageable);
}

