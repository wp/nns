package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.TopicFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicFileRepository extends JpaRepository<TopicFile,Long> {
}
