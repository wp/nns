package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.User;
import mk.ukim.finki.wp.nns.model.enumerations.UserRole;

public interface UserRepository extends JpaSpecificationRepository<User, String>
{
    User findByRole(UserRole role);
}
