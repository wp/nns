package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.TopicComment;

import java.util.List;
import java.util.Optional;

public interface CommentSerivce
{
    List<TopicComment> findAll();

    Optional<TopicComment> findById(Long id);

    TopicComment create(String commentText, Long topicId, String memberId);

    TopicComment update(Long id, String commentText);

    TopicComment delete(Long id);
}
