package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.CommissionMember;
import mk.ukim.finki.wp.nns.model.Professor;

import java.util.List;

public interface CommissionMemberService
{
    List<CommissionMember> findAll();

    List<CommissionMember> findAllByProfessor(String professorId);
}
