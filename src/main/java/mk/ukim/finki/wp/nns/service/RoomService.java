package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.Room;

import java.util.List;

public interface RoomService
{
    List<Room> findAll();
}
