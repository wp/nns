package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.Student;
import mk.ukim.finki.wp.nns.model.Topic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StudentService
{
    Student findById(String id);

    List<Student> findAll();

    public Page<Student> findAllWithPagination(Pageable pageable);

    public List<Topic> findAllTopicsWhereStudentIsMentionedSortedByMeetingDate(Student student);

    public List<Topic> findAllTopicsWhereStudentIsMentionedFilteredByCategoryNameAndSubCategoryName(Student student, String categoryName, String subCategoryName);
}
