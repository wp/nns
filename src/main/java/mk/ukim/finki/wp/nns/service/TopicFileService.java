package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.TopicFile;
import java.util.Optional;

public interface TopicFileService {
    Optional<TopicFile> findById(Long id);
}
