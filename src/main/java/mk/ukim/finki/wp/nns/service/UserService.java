package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.User;

public interface UserService
{
    User findById(String id);
}
