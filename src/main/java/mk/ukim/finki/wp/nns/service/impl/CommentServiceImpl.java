package mk.ukim.finki.wp.nns.service.impl;

import mk.ukim.finki.wp.nns.model.TopicComment;
import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.Topic;
import mk.ukim.finki.wp.nns.repository.CommentRepository;
import mk.ukim.finki.wp.nns.repository.ProfessorRepository;
import mk.ukim.finki.wp.nns.repository.TopicRepository;
import mk.ukim.finki.wp.nns.service.CommentSerivce;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentSerivce
{

    private final CommentRepository commentRepository;
    private final TopicRepository topicRepository;
    private final ProfessorRepository professorRepository;

    public CommentServiceImpl(CommentRepository commentRepository, TopicRepository topicRepository, ProfessorRepository professorRepository)
    {
        this.commentRepository = commentRepository;
        this.topicRepository = topicRepository;
        this.professorRepository = professorRepository;
    }


    @Override
    public List<TopicComment> findAll()
    {
        return commentRepository.findAll();
    }

    @Override
    public Optional<TopicComment> findById(Long id)
    {
        return commentRepository.findById(id);
    }

    @Override
    public TopicComment create(String commentText, Long topicId, String memberId)
    {
        Professor professor = professorRepository.findById(memberId).orElseThrow();
        Topic topic = topicRepository.getById(topicId);

        TopicComment comment = new TopicComment(commentText, topic, LocalDateTime.now(), professor);

        return commentRepository.save(comment);
    }

    @Override
    public TopicComment update(Long id, String commentText)
    {
        TopicComment comment = this.findById(id).orElseThrow();

        comment.setCommentText(commentText);
        comment.setDatePosted(LocalDateTime.now());

        return commentRepository.save(comment);
    }

    @Override
    public TopicComment delete(Long id) {

        TopicComment comment = this.findById(id).orElseThrow();

        commentRepository.delete(comment);

        return comment;
    }

}
