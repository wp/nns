package mk.ukim.finki.wp.nns.web;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.nns.config.FacultyUserDetails;
import mk.ukim.finki.wp.nns.model.*;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import mk.ukim.finki.wp.nns.service.*;
import org.jodconverter.core.office.OfficeException;
import org.json.JSONException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;


@Controller
@RequestMapping(value = {"/", "/nns-meetings"})
@RequiredArgsConstructor
public class MeetingController {
    private final MeetingService meetingService;
    private final ProfessorService professorService;
    private final StudentService studentService;
    private final TopicService topicService;
    private final RoomService roomService;
    private final CommissionMemberService commissionMemberService;
    private final UserService userService;


    @GetMapping
    public String getMeetingsPage(Model model,
                                  @RequestParam(required = false) List<TeachingAndScientificMeeting> filteredMeetingsDesc,
                                  @RequestParam(required = false) LocalDateTime date,
                                  @RequestParam(name = "page", defaultValue = "0") String pageString,
                                  @RequestParam(name = "size", defaultValue = "5") int size)
    {

        int page;

        try
        {
            page = Integer.parseInt(pageString);
        }
        catch (NumberFormatException e)
        {
            page = 0;
        }

        PageRequest pageable = PageRequest.of(page, size);
        Page<TeachingAndScientificMeeting> pagedData = meetingService.findAllWithPagination(pageable);

        if (filteredMeetingsDesc != null && filteredMeetingsDesc.size() == meetingService.findAllSortedByDateDesc().size())
            model.addAttribute("pagedData", pagedData);

        if(filteredMeetingsDesc == null)
            model.addAttribute("pagedData", pagedData);

        List<TeachingAndScientificMeeting> meetingsDesc = meetingService.findAllSortedByDateDesc();

        if (meetingsDesc != null && !meetingsDesc.isEmpty()) {
            model.addAttribute("latest", meetingsDesc.get(0));
        }
        if (filteredMeetingsDesc == null)
        {
            model.addAttribute("meetings", meetingsDesc);
            model.addAttribute("selectedDate", null);
        }
        else
        {
            model.addAttribute("meetings", filteredMeetingsDesc);
            model.addAttribute("selectedDate", date);
        }

        model.addAttribute("startFrom",size*(Integer.parseInt(pageString)));
        model.addAttribute("title", "ННС Седници");
       // model.addAttribute("df", DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
        model.addAttribute("bodyContent", "nns-meetings-page");

        return "master-template";
    }

    @PostMapping
    public String findAllHeldBeforeSelectedDateDesc(Model model,
                                                    @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime date,
                                                    @RequestParam(name = "page", defaultValue = "0") String pageString,
                                                    @RequestParam(name = "size", defaultValue = "5") int size)
    {

        if(date!=null)
            return getMeetingsPage(model, meetingService.findAllHeldBeforeSelectedDateDesc(date), date, pageString, size);
        else
            return getMeetingsPage(model, null,null, "0",size);
    }

    @GetMapping("/add")
    public String getMeetingAddPage(Model model) {

        List<Room> rooms = roomService.findAll();

        model.addAttribute("title", "Додади Нова Седница");
        model.addAttribute("rooms", rooms);
        model.addAttribute("bodyContent", "nns-meeting-add");

        return "master-template";
    }

    @PostMapping("/add")
    public String createMeeting(@RequestParam String meetingNumber,
                                @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime date,
                                @RequestParam String room)
    {
        meetingService.create(meetingNumber, date, room);

        return "redirect:/nns-meetings";

    }
    @GetMapping("{id}/edit")
    public String editMeeting(Model model,
                              @PathVariable String id)
    {
        TeachingAndScientificMeeting meeting = meetingService.findById(id);
        List<Room> rooms = roomService.findAll();

        model.addAttribute("meeting", meeting);
        model.addAttribute("rooms", rooms);
        model.addAttribute("title", "Изменете Запис");
        model.addAttribute("bodyContent", "nns-meeting-edit");

        return "master-template";
    }

    @PostMapping("/{id}/edit")
    public String editMeeting(@RequestParam String meetingNumber,
                              @RequestParam LocalDateTime date,
                              @RequestParam String room,
                              @RequestParam(required = false) Integer presentMembers,
                              @RequestParam(required = false) Integer eligibleMembers)
    {
        meetingService.update(meetingNumber, date, room, presentMembers, eligibleMembers);
        TeachingAndScientificMeeting meeting = meetingService.findById(meetingNumber);

        String meetingNumberEncoded = meeting.getMeetingNumberEncoded();
        meetingNumberEncoded = meetingNumberEncoded.replace("+", "%20");

        return "redirect:/nns-meetings/"+ meetingNumberEncoded +"/topics-list";
    }

    @GetMapping("/{id}/add-topic")
    public String showAddTopicsPage(Model model,
                                    @PathVariable String id)
    {
        TeachingAndScientificMeeting nnsMeeting = meetingService.findById(id);
        List<Professor> professors = professorService.findAll();
        List<Student> students = studentService.findAll();
        Set<String> subCategories = topicService.getAllSubCategories();

        model.addAttribute("nnsMeeting", nnsMeeting);
        model.addAttribute("professors", professors);
        model.addAttribute("students", students);
        model.addAttribute("student", "");
        model.addAttribute("topicCategories", TopicCategory.values());
        model.addAttribute("subCategories", subCategories);
        model.addAttribute("title", "Додадете Нов Запис");
        model.addAttribute("bodyContent", "add-topic-page");

        return "master-template";
    }

    @GetMapping("/{id}/edit-topic/{topicId}")
    public String getEditTopicPage(Model model,
                                   @PathVariable String id,
                                   @PathVariable Long topicId)
    {
        TeachingAndScientificMeeting meeting = meetingService.findById(id);
        List<Professor> professors = professorService.findAll();
        List<Student> students = studentService.findAll();
        Set<String> subCategories = topicService.getAllSubCategories();
        Topic topic = topicService.findById(topicId);

        model.addAttribute("meeting", meeting);
        model.addAttribute("professors", professors);
        model.addAttribute("students", students);
        model.addAttribute("student", topic.getMentionedStudent());
        model.addAttribute("topicCategories", TopicCategory.values());
        model.addAttribute("subCategories", subCategories);
        model.addAttribute("topic", topic);
        model.addAttribute("title", "Сменете Запис");
        model.addAttribute("bodyContent", "add-topic-page");

        return "master-template";
    }

    @GetMapping("/{id}/topics-list")
    public String showListTopicsPage(Model model, @PathVariable String id, Authentication token)
    {
        TeachingAndScientificMeeting meeting = meetingService.findById(id);
        List<Topic> topics = meetingService.sortAcceptedTopicsBySerialNumberForMeeting(id);

        if(!topics.isEmpty()){
            Long topicId = topics.get(0).getId();
            model.addAttribute("topicId", topicId);
        }

        Object principal = token.getPrincipal();
        FacultyUserDetails user = (FacultyUserDetails) principal;
        String userId = user.getUsername();

        User userLogged = userService.findById(userId);

        Long requestedTopicsCount = topicService.countRequests(id);

        List<CommissionMember> commission = commissionMemberService.findAllByProfessor(userId);

        model.addAttribute("meeting", meeting);
        model.addAttribute("topics", topics);
        model.addAttribute("user", userLogged);
        model.addAttribute("requestedTopicsCount", requestedTopicsCount);
        model.addAttribute("title", "Преглед На Седница");
        model.addAttribute("bodyContent", "list-topics");

        return "master-template";
    }

    @GetMapping("/{id}/topics-list/report")
    public String showMeetingReportPage(Model model, @PathVariable String id){
        TeachingAndScientificMeeting meeting = meetingService.findById(id);
        List<Topic> topics = meetingService.sortAcceptedTopicsBySerialNumberForMeeting(id);

        model.addAttribute("meeting", meeting);
        model.addAttribute("topics", topics);

        return "preview-meeting-report";
    }

    @GetMapping("/{id}/requested-topics-list")
    public String showListRequestedTopicsPage(Model model, @PathVariable String id, Authentication token)
    {
        TeachingAndScientificMeeting meeting = meetingService.findById(id);
        List<Topic> topics = meetingService.sortRequestedTopicsBySerialNumberForMeeting(id);

        Object principal = token.getPrincipal();
        FacultyUserDetails user = (FacultyUserDetails) principal;
        String userId = user.getUsername();

        User userLogged = userService.findById(userId);

        List<CommissionMember> commission = commissionMemberService.findAllByProfessor(userId);

        model.addAttribute("meeting", meeting);
        model.addAttribute("topics", topics);
        model.addAttribute("user", userLogged);
        model.addAttribute("title", "Преглед На Седница");
        model.addAttribute("bodyContent", "list-requested-topics");

        return "master-template";
    }

    @GetMapping("{id}/delete")
    public String showDeleteMeetingPage(Model model,
                                        @PathVariable String id)
    {
        TeachingAndScientificMeeting meeting = meetingService.findById(id);

        model.addAttribute("meeting",meeting);
        model.addAttribute("bodyContent", "nns-meetings-delete");

        return "master-template";
    }

    @PostMapping("{id}/delete")
    public String deleteTopic(@PathVariable String id)
    {
        TeachingAndScientificMeeting meeting = meetingService.remove(id);

        return "redirect:/nns-meetings";
    }


    @PostMapping("/upload")
    public String uploadMeeting(@RequestParam("meetingFile") MultipartFile meetingFile, RedirectAttributes redirectAttributes) throws JSONException, IOException, OfficeException {
        if (meetingFile.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "No file selected for upload.");
            return "redirect:/nns-meetings";
        }
        meetingService.processUploadedFile(meetingFile);
        redirectAttributes.addFlashAttribute("message", "Successfully uploaded: " + meetingFile.getOriginalFilename());
        return "redirect:/nns-meetings";
    }
}
