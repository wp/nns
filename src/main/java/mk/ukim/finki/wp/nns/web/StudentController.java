package mk.ukim.finki.wp.nns.web;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.Student;
import mk.ukim.finki.wp.nns.model.Topic;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import mk.ukim.finki.wp.nns.service.StudentService;
import mk.ukim.finki.wp.nns.service.TopicService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/students")
@AllArgsConstructor
public class StudentController {
    private final StudentService studentService;
    private final TopicService topicService;

    @GetMapping
    public String getStudentsPage(Model model,
                                  @RequestParam(required = false) Student studentOfInterest,
                                  @RequestParam(required = false) List<Topic> topicsThatIncludeStudentOfInterest,
                                  @RequestParam(name = "page", defaultValue = "0") String pageString,
                                  @RequestParam(name = "size", defaultValue = "5") int size){
        int page;
        pageString = "0";

        try
        {
            page = Integer.parseInt(pageString);
        }
        catch (NumberFormatException e)
        {
            page = 0;
        }

        PageRequest pageable = PageRequest.of(page, size);
        Page<Student> pagedData = studentService.findAllWithPagination(pageable);

        if(!pageString.isEmpty() && studentOfInterest == null){
            model.addAttribute("pagedData", pagedData);
        }

        boolean showTopics = false;
        List<Student> studentsInFilter = studentService.findAll();
        List<Student> studentsToBeShown = studentService.findAll();

        if(topicsThatIncludeStudentOfInterest != null){
            showTopics = true;

            model.addAttribute("topics", topicsThatIncludeStudentOfInterest);
            model.addAttribute("topicCategories", TopicCategory.values());
            model.addAttribute("subCategories", topicService.getAllSubCategories());
        }

        if(studentOfInterest == null){
            model.addAttribute("studentsToBeShown", studentsToBeShown);
        }

        else{
            model.addAttribute("studentsToBeShown", studentsToBeShown
                    .stream()
                    .filter(student -> student.getIndex().equals(studentOfInterest.getIndex()))
                    .toList());
        }

        model.addAttribute("studentOfInterest", studentOfInterest);
        model.addAttribute("studentsInFilter", studentsInFilter);

        model.addAttribute("showTopics", showTopics);


        model.addAttribute("startFrom",size*(Integer.parseInt(pageString)));
        model.addAttribute("title", "Студенти");
        model.addAttribute("bodyContent", "students-page");

        return "master-template";
    }

    @PostMapping
    public String filterByStudent(Model model,
                                  @RequestParam String studentOfInterest) {
        if(!studentOfInterest.equalsIgnoreCase("Сите")){
            return getStudentsPage(model, studentService.findById(studentOfInterest), null, "", 5);
        }

        return getStudentsPage(model, null, null, "", 5);
    }

    @GetMapping("/{id}/topics-list")
    public String showTopicsWhereStudentIsMentionedSortedByMeetingDate(Model model,
                                                                       @PathVariable String id){
        Student studentOfInterest = studentService.findById(id);
        List<Topic> filteredTopicsForStudentOfInterest = studentService.findAllTopicsWhereStudentIsMentionedSortedByMeetingDate(studentOfInterest);

        return getStudentsPage(model, studentOfInterest, filteredTopicsForStudentOfInterest, "", 5);
    }

    @PostMapping("/{id}/topics-list")
    public String showTopicsForStudentFilteredBySpecs(Model model,
                                                      @PathVariable String id,
                                                      @RequestParam(required = false) String categoryName,
                                                      @RequestParam(required = false) String subCategoryName){
        Student studentOfInterest = studentService.findById(id);
        List<Topic> filteredTopicsForStudentOfInterest = studentService.findAllTopicsWhereStudentIsMentionedFilteredByCategoryNameAndSubCategoryName(studentOfInterest, categoryName, subCategoryName);

        if (!categoryName.equalsIgnoreCase("Сите")){
            model.addAttribute("selectedCategory", TopicCategory.valueOf(categoryName));
        }

        model.addAttribute("selectedSubCategory", subCategoryName);

        return getStudentsPage(model, studentOfInterest, filteredTopicsForStudentOfInterest, "", 5);
    }
}
